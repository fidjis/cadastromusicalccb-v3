import 'dart:collection';

class EventModel {

  String idEvent;
  String nameEvent;
  String passEvent;
  String localEvent;
  String dateEvent;

  EventModel({this.nameEvent, this.passEvent, this.localEvent, this.dateEvent, this.idEvent});

  HashMap<String, String> getEventMap(){
    HashMap<String, String> map = HashMap();
    map["idEvent"] = idEvent;
    map["nameEvent"] = nameEvent;
    map["passEvent"] = passEvent;
    map["localEvent"] = localEvent;
    map["dateEvent"] = dateEvent;

    return map;
  }
}