import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class EventInfo {

  static const String ANCIAO_NAME = "ANCIAO_NAME";
  static const String ENCARREGADO_NAME = "ENCARREGADO_NAME";
  static const String PALAVRA = "PALAVRA";
  static const String HINOS_TOCADOS = "HINOS_TOCADOS";

  String anciaoName;
  String engarregado;
  String palavra;
  List<int> hinosTocados;

  Map<String, dynamic> dataMap;

  toMap(){
    dataMap = {
      ANCIAO_NAME: anciaoName,
      ENCARREGADO_NAME: engarregado,
      PALAVRA: palavra,
      HINOS_TOCADOS: hinosTocados,
    };
  }

  getEventInf(String eventID){
    Firestore.instance.collection("v3").document("data").collection("events_info")
        .document(eventID).collection("geral_info").document("inf_culto").snapshots().listen(
            (doc){
              anciaoName = doc[ANCIAO_NAME];
              engarregado = doc[ENCARREGADO_NAME];
              palavra = doc[PALAVRA];
            });
  }

  save(String eventID, BuildContext context) async {

    toMap();
    await Firestore.instance.collection("v3").document("data").collection("events_info")
        .document(eventID).collection("geral_info").document("inf_culto").setData(dataMap).then(
            (s){
          Scaffold.of(context).showSnackBar(SnackBar(content: Text('Salvo!'), duration: Duration(seconds: 2),));
        }).catchError((error){
      Scaffold.of(context).showSnackBar(SnackBar(content: Text('Falha'), duration: Duration(seconds: 2),));
    });
  }
}