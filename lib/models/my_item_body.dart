import 'package:flutter/material.dart';

class MyItemBody extends StatefulWidget {

  MyItemBody({this.name});

  final String name;
  final _MyItemBodyState state = _MyItemBodyState();

  @override
  _MyItemBodyState createState() => state;
}

class _MyItemBodyState extends State<MyItemBody> {

  int _count = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black54,
      child: ListTile(
        contentPadding: EdgeInsets.symmetric(horizontal: 20.0,),
        leading: Container(
          padding: EdgeInsets.only(right: 12.0),
          decoration: BoxDecoration(
              border: Border(
                  right: BorderSide(width: 1.0, color: Colors.white24))),
          child: Icon(Icons.people, color: Colors.white),
        ),
        title: Text(
          widget.name,
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
        trailing:  Text(
          "$_count",
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
        ),
      ),
    );
  }

  addCount(){
    setState(() {
      _count++;
    });
  }
}
