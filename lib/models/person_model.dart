import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

abstract class Person{
  ///CONSTANTES
  static const String MUSICO = 'MUSICO';
  static const String ORGANISTA = 'ORGANISTA';
  static const String MINISTERIO = 'MINISTERIO';

  static const String INSTRUTORA = 'Instrutora';
  static const String EXAMINADORA = 'Examinadora';
  static const String CULTO_OFICIAL = 'Culto Oficial';
  static const String RJM = 'RJM';

  static const String ENC_LOCAL = 'Enc. Local';
  static const String ENC_REGIONAL = 'Enc. Regional';
  static const String ANCIAO = 'Ancião';
  static const String INSTRUTOR = 'Instrutor';
  static const String COOP_OFICIAL = 'Coop. Oficial';
  static const String COOP_JOVENS = 'Coop. Jovens';
  static const String DIACONO = 'Diacono';

  ///VARIAVEIS HERDADAS
  String TAG; //musico ou organista
  String cidade = "";
  Map cargos;
  Map<String, dynamic> dataMap;

  toMap(){}

  save(String eventID, BuildContext context){}
}

class Organista implements Person{

  String instrumento = "";
  bool oficializado = false;

  @override
  String TAG = "";

  @override
  Map cargos = {
    Person.EXAMINADORA: false,
    Person.INSTRUTORA: false,
    Person.CULTO_OFICIAL: false,
    Person.RJM: false,
  };

  @override
  String cidade = "";

  @override
  Map<String, dynamic> dataMap;

  @override
  save(String eventID, BuildContext context) async {

    TAG = Person.ORGANISTA;
    instrumento = "Orgão";

    toMap(); //SETANDO OS VALORES

    await Firestore.instance.collection("v3").document("data").collection("events_info")
        .document(eventID).collection("persons").document().setData(dataMap).then(
            (s){
          Scaffold.of(context).showSnackBar(SnackBar(content: Text('Adicionado'), duration: Duration(seconds: 2),));
        }).catchError((error){
          Scaffold.of(context).showSnackBar(SnackBar(content: Text('Falha'), duration: Duration(seconds: 2),));
    });
  }

  @override
  toMap() {
    dataMap = {
      "TAG": TAG,
      "cidade": cidade,
      "instrumento": instrumento,
      "oficializado": oficializado,
      "cargos": cargos,
    };
    return dataMap;
  }

}

class Musico implements Person{

  String instrumento = "";
  bool oficializado = false;

  @override
  String TAG = "";

  @override
  Map cargos = {
    Person.ENC_LOCAL: false,
    Person.ENC_REGIONAL: false,
    Person.ANCIAO: false,
    Person.INSTRUTOR: false,
    Person.DIACONO: false,
    Person.COOP_JOVENS: false,
    Person.COOP_OFICIAL: false,
  };

  @override
  String cidade = "";

  @override
  Map<String, dynamic> dataMap;

  @override
  save(String eventID, BuildContext context) async {

    TAG = Person.MUSICO;

    toMap(); //SETANDO OS VALORES

    await Firestore.instance.collection("v3").document("data").collection("events_info")
        .document(eventID).collection("persons").document().setData(dataMap).then(
            (s){
          Scaffold.of(context).showSnackBar(SnackBar(content: Text('Adicionado'), duration: Duration(seconds: 2),));
        }).catchError((error){
      Scaffold.of(context).showSnackBar(SnackBar(content: Text('Falha'), duration: Duration(seconds: 2),));
    });
  }

  @override
  toMap(){
    dataMap = {
      "TAG": TAG,
      "cidade": cidade,
      "instrumento": instrumento,
      "oficializado": oficializado,
      "cargos": cargos,
    };
    return dataMap;
  }

}

class Ministerio implements Person{

  @override
  String TAG = "";

  @override
  Map cargos = {
    Person.ANCIAO: false,
    Person.DIACONO: false,
    Person.COOP_JOVENS: false,
    Person.COOP_OFICIAL: false,
  };

  @override
  String cidade = "";

  @override
  Map<String, dynamic> dataMap;

  @override
  save(String eventID, BuildContext context) async {

    TAG = Person.MINISTERIO;

    toMap(); //SETANDO OS VALORES

    await Firestore.instance.collection("v3").document("data").collection("events_info")
        .document(eventID).collection("persons").document().setData(dataMap).then(
            (s){
          Scaffold.of(context).showSnackBar(SnackBar(content: Text('Adicionado'), duration: Duration(seconds: 2),));
        }).catchError((error){
      Scaffold.of(context).showSnackBar(SnackBar(content: Text('Falha'), duration: Duration(seconds: 2),));
    });
  }

  @override
  toMap() {
    dataMap = {
      "TAG": TAG,
      "cidade": cidade,
      "cargos": cargos,
    };
    return dataMap;
  }

}