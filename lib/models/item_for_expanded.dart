import 'package:flutter/material.dart';

class MyItemExpanded {
  MyItemExpanded({this.TAG, this.isExpanded: false, this.header, this.body });

  final String TAG;
  bool isExpanded;
  final Widget header;
  final Widget body;
}