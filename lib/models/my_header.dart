import 'package:flutter/material.dart';

class MyHeader extends StatefulWidget {

  MyHeader({this.headerName});

  final String headerName;
  final _MyHeaderState state = _MyHeaderState();

  @override
  _MyHeaderState createState() => state;
}

class _MyHeaderState extends State<MyHeader> {

  int _count = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
          leading: Container(
            padding: EdgeInsets.only(right: 12.0),
            decoration: BoxDecoration(
                border: Border(
                    right: BorderSide(width: 1.0, color: Colors.black))),
            child: Icon(Icons.music_note, color: Colors.black),
          ),
          title: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(right: 15.0),
                decoration: BoxDecoration(
                    border: Border(
                        right: BorderSide(width: 1.0, color: Colors.black))),
                child: Text(
                  "$_count",
                  style: TextStyle(fontSize:21.0, color: Colors.black, fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(width: 12.0,),
              Text(
                widget.headerName,
                style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
              ),
            ],
          )
      ),
    );
  }

  addCount(){
    setState(() {
      _count++;
    });
  }
}



