import 'package:flutter/material.dart';

class InstrumentItem extends StatefulWidget {
  @override

  InstrumentItem({this.quant, this.name, this.isVisible});

  int quant;
  String name;
  bool isVisible;

  _InstrumentItemState instrumentItemState = _InstrumentItemState();

  _InstrumentItemState createState() => instrumentItemState;
}

class _InstrumentItemState extends State<InstrumentItem> {
  @override
  Widget build(BuildContext context) {
    return Visibility(
      maintainState: true,
      visible: widget.isVisible,
      child:  Container(
        color: Colors.black54,
        child: ListTile(
          contentPadding: EdgeInsets.symmetric(horizontal: 20.0,),
          leading: Container(
            padding: EdgeInsets.only(right: 12.0),
            decoration: BoxDecoration(
                border: Border(
                    right: BorderSide(width: 1.0, color: Colors.white24))),
            child: Icon(Icons.people, color: Colors.white),
          ),
          title: Text(
            "${widget.name}",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
          trailing:  Text(
            "${widget.quant}",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }

  addCount(){
    setState(() {
      widget.quant++;
    });
    if(!widget.isVisible)
      _setVisibility();
  }

  _setVisibility(){
    setState(() {
      widget.isVisible = true;
    });
  }
}