import 'dart:async';

import 'package:cadastro_musical_ccb_v3/models/event_model.dart';
import 'package:cadastro_musical_ccb_v3/models/instrument_item.dart';
import 'package:cadastro_musical_ccb_v3/models/item_for_expanded.dart';
import 'package:cadastro_musical_ccb_v3/models/my_header.dart';
import 'package:cadastro_musical_ccb_v3/models/my_item_body.dart';
import 'package:cadastro_musical_ccb_v3/models/person_model.dart';
import 'package:cadastro_musical_ccb_v3/screens/add_informacoes_screen.dart';
import 'package:cadastro_musical_ccb_v3/screens/add_ministerio_screen.dart';
import 'package:cadastro_musical_ccb_v3/screens/add_musico_screen.dart';
import 'package:cadastro_musical_ccb_v3/screens/add_organista_screen.dart';
import 'package:cadastro_musical_ccb_v3/screens/config_screen.dart';
import 'package:cadastro_musical_ccb_v3/util/diagonal_cliper.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class EventDataHome extends StatefulWidget {

  EventDataHome({this.EVENT});

  final EventModel EVENT;

  @override
  _EventDataHomeState createState() => _EventDataHomeState();
}

class _EventDataHomeState extends State<EventDataHome> {

  MyHeader headerIntrument;
  MyHeader headerMusico;
  MyHeader headerOrganista;
  MyHeader headerMinisterio;

  //MUSICOS
  MyItemBody itemBodyRegionais;
  MyItemBody itemBodyInstrutores;
  MyItemBody itemBodyOficializados;
  MyItemBody itemBodyNaoOficializados;
  MyItemBody itemBodyMinisterio;

  //ORGANISTAS
  MyItemBody itemBodyInstrutoras;
  MyItemBody itemBodyExaminadoras;
  MyItemBody itemBodyOficializadas;
  MyItemBody itemBodyNaoOficializadas;
  MyItemBody itemBodyCultoOficial;
  MyItemBody itemBodyRJM;

  //MINISTERIO
  MyItemBody itemBodyAnciao;
  MyItemBody itemBodyDiacono;
  MyItemBody itemBodyCoopJovens;
  MyItemBody itemBodyCoopOficial;

  StreamSubscription<QuerySnapshot> streamSub; ///CONTROLER O STREM DE DADOS

  List<MyItemExpanded> _items = List<MyItemExpanded>();

  List<Widget> _instrumentosWidgets = List<Widget>();

  InstrumentItem violinoWidget;
  InstrumentItem violaWidget;
  InstrumentItem violonceloWidget;
  InstrumentItem flautaWidget;
  InstrumentItem acordeonWidget;
  InstrumentItem clarinetaWidget;
  InstrumentItem oboeWidget;
  InstrumentItem corneInglesWidget;
  InstrumentItem fagoteWidget;
  InstrumentItem claroneWidget;
  InstrumentItem saxSopranoWidget;
  InstrumentItem saxAltoWidget;
  InstrumentItem saxTenorWidget;
  InstrumentItem saxBaritonoWidget;
  InstrumentItem trompeteWidget;
  InstrumentItem cornetWidget;
  InstrumentItem flugelHornWidget;
  InstrumentItem melofoneWidget;
  InstrumentItem trompaWidget;
  InstrumentItem tubaWagerianaWidget;
  InstrumentItem tromboneWidget;
  InstrumentItem trombonitoWidget;
  InstrumentItem baritonoWidget;
  InstrumentItem eufonioWidget;
  InstrumentItem bombardinoWidget;
  InstrumentItem tubaWidget;
  InstrumentItem outroWidget;

  @override
  void initState() {
    super.initState();
    _buildExpandedItemInstrumentos();
    _buildExpandedItemMusicos();
    _buildExpandedItemOrganistas();
    _buildExpandedItemMinisterio();

    initializeStream();
  }

  @override
  void dispose() {
    streamSub.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.EVENT.nameEvent),
        centerTitle: true,
        backgroundColor: Colors.orangeAccent,
        actions: <Widget>[
          PopupMenuButton(
            icon: Icon(Icons.linear_scale, color: Colors.white),
            itemBuilder: (context) => <PopupMenuItem<String>>[
              PopupMenuItem<String>(child: const Text('Excluir Evento'), value: 'excluir'),
              PopupMenuItem<String>(child: const Text('Compartilhar'), value: 'compartilhar'),
            ],
            onSelected: (choice){
              switch(choice){
                case 'excluir':
                  excluirEsteEvento();
                  break;
                case 'compartilhar':
                  break;
              }
            },
          )
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        child: RaisedButton(
          onPressed: _buildBottomSheet,
          color: Colors.orangeAccent,
          child: Container(
            height: 40.0,
            margin: EdgeInsets.all(5.0),
            child: Row(
              children: <Widget>[
                Icon(Icons.keyboard_arrow_up, color: Colors.white, size: 37.0),
                Expanded(child: Text(
                  "Click para administrar o Evento",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 16.0, color: Colors.white),
                )),
                Icon(Icons.keyboard_arrow_up, color: Colors.white, size: 37.0),
              ],
            ),
          ),
        ),
      ),
      body: Stack(
        children: <Widget>[
          ClipPath(
            clipper: DialogonalClipper(),
            child: Image.asset(
              'background_poligono.jpg',
              fit: BoxFit.fitHeight,
              colorBlendMode: BlendMode.srcOver,
              color: Color.fromARGB(120, 20, 10, 40),
            ),
          ),
          ListView(
            padding: EdgeInsets.only(bottom: 70.0, top: 14.0, left: 14.0, right: 14.0),
            children: [
              ExpansionPanelList(
                expansionCallback: (int index, bool isExpanded) {
                  setState(() {
                    _items[index].isExpanded = !_items[index].isExpanded;
                  });
                },
                children: _items.map((MyItemExpanded item) {
                  return ExpansionPanel(
                    headerBuilder: (BuildContext context, bool isExpanded) {
                      return item.header;
                    },
                    isExpanded: item.isExpanded,
                    body: item.body,
                  );
                }).toList(),
              ),
            ],
          )
        ],
      ),
    );
  }

  initializeStream() async {

    streamSub = Firestore.instance.collection("v3").document("data")
        .collection("events_info").document(widget.EVENT.idEvent).collection("persons").snapshots()
        .listen((querySnapshot) {
          querySnapshot.documentChanges.forEach((change) {

            String instrumentName = change.document['instrumento'];
            String TAG = change.document['TAG'];
            bool oficializado = change.document['oficializado'];
            bool vaiTocar = true;
            Map cargos = change.document['cargos'];

            cargos.forEach((key, value){
              if(value){

                switch(key){
                  case Person.ENC_REGIONAL:
                    itemBodyRegionais.state.addCount();
                    break;
                  case Person.INSTRUTOR:
                    itemBodyInstrutores.state.addCount();
                    break;
                  case Person.ANCIAO:
                    itemBodyAnciao.state.addCount();
                    if(TAG == Person.MUSICO)
                      itemBodyMinisterio.state.addCount();
                    break;
                  case Person.DIACONO:
                    itemBodyDiacono.state.addCount();
                    if(TAG == Person.MUSICO)
                      itemBodyMinisterio.state.addCount();
                    break;
                  case Person.COOP_JOVENS:
                    itemBodyCoopJovens.state.addCount();
                    if(TAG == Person.MUSICO)
                      itemBodyMinisterio.state.addCount();
                    break;
                  case Person.COOP_OFICIAL:
                    itemBodyCoopOficial.state.addCount();
                    if(TAG == Person.MUSICO)
                      itemBodyMinisterio.state.addCount();
                    break;
                  case Person.INSTRUTORA:
                    itemBodyInstrutoras.state.addCount();
                    vaiTocar = false;
                    break;
                  case Person.EXAMINADORA:
                    itemBodyExaminadoras.state.addCount();
                    vaiTocar = false;
                    break;
                  case Person.CULTO_OFICIAL:
                    itemBodyCultoOficial.state.addCount();
                    break;
                  case Person.RJM:
                    itemBodyRJM.state.addCount();
                    break;
                }
              }
            });

            if(TAG == Person.MUSICO)
              setState(() {

                if(oficializado != null && oficializado)
                  itemBodyOficializados.state.addCount();
                else if(oficializado != null && !oficializado)
                  itemBodyNaoOficializados.state.addCount();

                headerIntrument.state.addCount();
                headerMusico.state.addCount();
              });
            if(TAG == Person.ORGANISTA)
              setState(() {

                if(oficializado != null && oficializado && vaiTocar)
                  itemBodyOficializadas.state.addCount();
                else if(oficializado != null && !oficializado && vaiTocar)
                  itemBodyNaoOficializadas.state.addCount();


                if(vaiTocar)
                  headerOrganista.state.addCount();
              });
            if(TAG == Person.MINISTERIO)
              setState(() {
                headerMinisterio.state.addCount();
              });

            switch(instrumentName){
              case "Violino":
                violinoWidget.instrumentItemState.addCount();
                break;
              case "Viola":
                violaWidget.instrumentItemState.addCount();
                break;
              case "Violoncelo":
                violonceloWidget.instrumentItemState.addCount();
                break;
              case "Flauta":
                flautaWidget.instrumentItemState.addCount();
                break;
              case "Acordeon":
                acordeonWidget.instrumentItemState.addCount();
                break;
              case "Clarineta":
                clarinetaWidget.instrumentItemState.addCount();
                break;
              case "Oboé":
                oboeWidget.instrumentItemState.addCount();
                break;
              case "Corne Inglês":
                corneInglesWidget.instrumentItemState.addCount();
                break;
              case "Fagote":
                fagoteWidget.instrumentItemState.addCount();
                break;
              case "Clarone":
                claroneWidget.instrumentItemState.addCount();
                break;
              case "Sax Soprano":
                saxSopranoWidget.instrumentItemState.addCount();
                break;
              case "Sax Alto":
                saxAltoWidget.instrumentItemState.addCount();
                break;
              case "Sax Tenor":
                saxTenorWidget.instrumentItemState.addCount();
                break;
              case "Sax Barítono":
                saxBaritonoWidget.instrumentItemState.addCount();
                break;
              case "Trompete":
                trompeteWidget.instrumentItemState.addCount();
                break;
              case "Cornet":
                cornetWidget.instrumentItemState.addCount();
                break;
              case "Flugel Horn":
                flugelHornWidget.instrumentItemState.addCount();
                break;
              case "Melofone":
                melofoneWidget.instrumentItemState.addCount();
                break;
              case "Trompa":
                trompaWidget.instrumentItemState.addCount();
                break;
              case "Tuba Wageriana":
                tubaWagerianaWidget.instrumentItemState.addCount();
                break;
              case "Trombone":
                tromboneWidget.instrumentItemState.addCount();
                break;
              case "Trombonito":
                trombonitoWidget.instrumentItemState.addCount();
                break;
              case "Barítono":
                baritonoWidget.instrumentItemState.addCount();
                break;
              case "Eufônio":
                eufonioWidget.instrumentItemState.addCount();
                break;
              case "Bombardino":
                bombardinoWidget.instrumentItemState.addCount();
                break;
              case "Tuba":
                tubaWidget.instrumentItemState.addCount();
                break;
              case "Outro":
                outroWidget.instrumentItemState.addCount();
                break;
            }
          });
        });
  }

  _buildExpandedItemInstrumentos(){

    headerIntrument = MyHeader(headerName: "Instrumentos",);

    violinoWidget = InstrumentItem(isVisible: false, name: "Violino", quant: 0,);
    violaWidget = InstrumentItem(isVisible: false, name: "Viola", quant: 0,);
    violonceloWidget = InstrumentItem(isVisible: false, name: "Violoncelo", quant: 0,);
    flautaWidget = InstrumentItem(isVisible: false, name: "Flauta", quant: 0,);
    acordeonWidget = InstrumentItem(isVisible: false, name: "Acordeon", quant: 0,);
    clarinetaWidget = InstrumentItem(isVisible: false, name: "Clarineta", quant: 0,);
    oboeWidget = InstrumentItem(isVisible: false, name: "Oboé", quant: 0,);
    corneInglesWidget = InstrumentItem(isVisible: false, name: "Corne Inglês", quant: 0,);
    fagoteWidget = InstrumentItem(isVisible: false, name: "Fagote", quant: 0,);
    claroneWidget = InstrumentItem(isVisible: false, name: "Clarone", quant: 0,);
    saxSopranoWidget = InstrumentItem(isVisible: false, name: "Sax Soprano", quant: 0,);
    saxAltoWidget = InstrumentItem(isVisible: false, name: "Sax Alto", quant: 0,);
    saxTenorWidget = InstrumentItem(isVisible: false, name: "Sax Tenor", quant: 0,);
    saxBaritonoWidget = InstrumentItem(isVisible: false, name: "Sax Barítono", quant: 0,);
    trompeteWidget = InstrumentItem(isVisible: false, name: "Trompete", quant: 0,);
    cornetWidget = InstrumentItem(isVisible: false, name: "Cornet", quant: 0,);
    flugelHornWidget = InstrumentItem(isVisible: false, name: "Flugel Horn", quant: 0,);
    melofoneWidget = InstrumentItem(isVisible: false, name: "Melofone", quant: 0,);
    trompaWidget = InstrumentItem(isVisible: false, name: "Trompa", quant: 0,);
    tubaWagerianaWidget = InstrumentItem(isVisible: false, name: "Tuba Wageriana", quant: 0,);
    tromboneWidget = InstrumentItem(isVisible: false, name: "Trombone", quant: 0,);
    trombonitoWidget = InstrumentItem(isVisible: false, name: "Trombonito", quant: 0,);
    baritonoWidget = InstrumentItem(isVisible: false, name: "Barítono", quant: 0,);
    eufonioWidget = InstrumentItem(isVisible: false, name: "Eufônio", quant: 0,);
    bombardinoWidget = InstrumentItem(isVisible: false, name: "Bombardino", quant: 0,);
    tubaWidget = InstrumentItem(isVisible: false, name: "Tuba", quant: 0,);
    outroWidget = InstrumentItem(isVisible: false, name: "Outro", quant: 0,);

    _instrumentosWidgets.add(violinoWidget);
    _instrumentosWidgets.add(tubaWidget);
    _instrumentosWidgets.add(violaWidget);
    _instrumentosWidgets.add(violonceloWidget);
    _instrumentosWidgets.add(flautaWidget);
    _instrumentosWidgets.add(clarinetaWidget);
    _instrumentosWidgets.add(acordeonWidget);
    _instrumentosWidgets.add(oboeWidget);
    _instrumentosWidgets.add(corneInglesWidget);
    _instrumentosWidgets.add(fagoteWidget);
    _instrumentosWidgets.add(claroneWidget);
    _instrumentosWidgets.add(saxSopranoWidget);
    _instrumentosWidgets.add(saxAltoWidget);
    _instrumentosWidgets.add(saxTenorWidget);
    _instrumentosWidgets.add(saxBaritonoWidget);
    _instrumentosWidgets.add(trompeteWidget);
    _instrumentosWidgets.add(cornetWidget);
    _instrumentosWidgets.add(flugelHornWidget);
    _instrumentosWidgets.add(melofoneWidget);
    _instrumentosWidgets.add(trompaWidget);
    _instrumentosWidgets.add(tubaWagerianaWidget);
    _instrumentosWidgets.add(tromboneWidget);
    _instrumentosWidgets.add(trombonitoWidget);
    _instrumentosWidgets.add(baritonoWidget);
    _instrumentosWidgets.add(eufonioWidget);
    _instrumentosWidgets.add(bombardinoWidget);
    _instrumentosWidgets.add(outroWidget);

    _items.add(MyItemExpanded(header: headerIntrument, body: Column(children: _instrumentosWidgets,)));
  }

  _buildExpandedItemMusicos(){

    headerMusico = MyHeader(headerName: "Músicos",);

    itemBodyRegionais = MyItemBody(name: "Regionais",);
    itemBodyInstrutores = MyItemBody(name: "Instrutores",);
    itemBodyOficializados = MyItemBody(name: "Oficializados",);
    itemBodyNaoOficializados = MyItemBody(name: "Nao Oficializados",);
    itemBodyMinisterio = MyItemBody(name: "Ministerio",);

    var body = Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        itemBodyRegionais,
        itemBodyInstrutores,
        itemBodyOficializados,
        itemBodyNaoOficializados,
        itemBodyMinisterio,
      ],
    );

    _items.add(MyItemExpanded(header: headerMusico, body: body));
  }

  _buildExpandedItemOrganistas(){

    headerOrganista = MyHeader(headerName: "Organistas",);

    itemBodyInstrutoras = MyItemBody(name: "Instrutoras",);
    itemBodyExaminadoras = MyItemBody(name: "Examinadoras",);
    itemBodyOficializadas = MyItemBody(name: "Oficializadas",);
    itemBodyNaoOficializadas = MyItemBody(name: "Nao Oficializadas",);
    itemBodyCultoOficial = MyItemBody(name: "Culto Oficial",);
    itemBodyRJM = MyItemBody(name: "RJM",);

    var body = Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        itemBodyInstrutoras,
        itemBodyExaminadoras,
        itemBodyOficializadas,
        itemBodyNaoOficializadas,
        itemBodyCultoOficial,
        itemBodyRJM
      ],
    );

    _items.add(MyItemExpanded(header: headerOrganista, body: body));
  }

  _buildExpandedItemMinisterio(){

    headerMinisterio = MyHeader(headerName: "Ministerio",);

    itemBodyAnciao = MyItemBody(name: "Ancião",);
    itemBodyDiacono = MyItemBody(name: "Diácono",);
    itemBodyCoopJovens = MyItemBody(name: "Coop. Jovens",);
    itemBodyCoopOficial = MyItemBody(name: "Coop. Oficial",);

    var body = Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        itemBodyAnciao,
        itemBodyDiacono,
        itemBodyCoopJovens,
        itemBodyCoopOficial,
      ],
    );

    _items.add(MyItemExpanded(header: headerMinisterio, body: body));
  }

  _buildBottomSheet() {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return Container(
            color: Colors.orangeAccent,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[

                SizedBox(height: 30.0,),
                Row(
                  children: <Widget>[
                    Expanded(child: _builBottomADD("Adicionar Músico", Icons.music_note, AddMusicoScreen(EVENT: widget.EVENT,))),
                    Expanded(child: _builBottomADD("Adicionar Organista", Icons.person, AddOrganistaScreen(EVENT: widget.EVENT,))),
                  ],
                ),
                SizedBox(height: 30.0,),
                Row(
                  children: <Widget>[
                    Expanded(child: _builBottomADD("Adicionar Ministerio", Icons.people, AddMinisterioScreen(EVENT: widget.EVENT,))),
                    Expanded(child: _builBottomADD("Adicionar Informações", Icons.info_outline, AddInformacoesScreen(EVENT: widget.EVENT,))),
                  ],
                ),
                SizedBox(height: 30.0,),
                Row(
                  children: <Widget>[
                    Expanded(child: _builBottomADD("Configurações", Icons.settings, ConfigScreen(EVENT: widget.EVENT,))),
                  ],
                ),
                SizedBox(height: 30.0,),
              ],
            ),
          );
        });
  }

  _builBottomADD(String label, IconData icon, var router) {
    return MaterialButton(
      onPressed: (){
        Navigator.of(context).pop();
        passWorldRequest(context, widget.EVENT, router);
      },
      child: Container(
        child: Column(
          children: <Widget>[
            Icon(icon, color: Colors.white, size: 50.0,),
            SizedBox(height: 4.0,),
            Text(label, style: TextStyle(
              color: Colors.white,
            ),)
          ],
        ),
      ),
    );
  }

  bool passWorldEnabled = false;
  Future<void> passWorldRequest(BuildContext context, EventModel event, var router) async {

    if(passWorldEnabled){
      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => router));
    }
    else{
      TextEditingController textEditingController = TextEditingController();
      String text = "";

      return showDialog<void>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Senha do Evento:'),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  TextField(
                    controller: textEditingController,
                    decoration: InputDecoration(
                      hintText: 'Senha',
                      contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(25.0)),
                    ),
                  ),
                  Center(child: Text(text, style: TextStyle(color: Colors.red)),),
                  MaterialButton(
                      color: Colors.orange,
                      minWidth: 160,
                      child: Text("Entrar", style: TextStyle(color: Colors.white),),
                      onPressed: () {

                        FocusScope.of(context).requestFocus(new FocusNode()); ///DIMISS KEYBORD
                        if(textEditingController.value.text == event.passEvent){
                          passWorldEnabled = true;
                          Navigator.pop(context);
                          Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => router));
                        }else{
                          setState(() {
                            text = "Senha Incorreta";
                          });
                        }
                      }
                  )
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('fechar', style: TextStyle(color: Colors.red),),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          );
        },
      );
    }
  }

  excluirEsteEvento() {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Tem certeza que deseja excluir o evento?'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                MaterialButton(
                    color: Colors.orange,
                    minWidth: 160,
                    child: Text("Sim", style: TextStyle(color: Colors.white),),
                    onPressed: () {

                      String id = widget.EVENT.idEvent;
                      Firestore.instance.collection("v3").document("data")
                          .collection("events").document(id).delete().whenComplete(
                          (){
                            Firestore.instance.collection("v3").document("data") //TODO: NAO ESTA APAGANDO
                                .collection("events_info").document(id).delete();
                          }
                      );
                      Navigator.pop(context);
                      Navigator.pop(context);
                    }
                )
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('fechar', style: TextStyle(color: Colors.red),),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );

  }
}