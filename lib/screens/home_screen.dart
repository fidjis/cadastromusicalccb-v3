import 'dart:async';

import 'package:cadastro_musical_ccb_v3/models/event_model.dart';
import 'package:cadastro_musical_ccb_v3/screens/event_data_home.dart';
import 'package:cadastro_musical_ccb_v3/util/dialog_new_event.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}


class _HomeScreenState extends State<HomeScreen> {

  final GlobalKey<ScaffoldState> _scaffoldKey2 = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey2,
      floatingActionButton: FloatingActionButton.extended(
        onPressed: _openNewEventDialogWithReturn,
        backgroundColor: Colors.orangeAccent,
        icon: Icon(Icons.format_line_spacing, color: Colors.white),
        label: Text("Novo Evento", style: TextStyle(color: Colors.white)),
      ),
      body: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                backgroundColor: Colors.orangeAccent,
                expandedHeight: 200.0,
                floating: false,
                pinned: true,
                actions: <Widget>[
                  PopupMenuButton(
                    icon: Icon(Icons.linear_scale, color: Colors.white),
                    itemBuilder: (context) => <PopupMenuItem<String>>[
                      PopupMenuItem<String>(child: const Text('Politica de Privacidade'), value: 'privacy_policy'),
                    ],
                    onSelected: (choice){
                      switch(choice){
                        case "privacy_policy":
                          Navigator.pushNamed(context, "/PrivacyPolictScreen");
                      }
                    },
                  )
                ],
                flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  title: Text("Cadastro Músical CCB\n       VERSÃO BETA!",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                      )),
                  background: Image.asset(
                    'background_poligono.jpg',
                    fit: BoxFit.cover,
                    colorBlendMode: BlendMode.srcOver,
                    color: Color.fromARGB(120, 20, 10, 40),
                  ),
                ),
              ),
            ];
          },
          body: Padding(
            padding: EdgeInsets.only(left: 5.0, right: 5.0,),
            child: StreamBuilder<QuerySnapshot>(
              stream: Firestore.instance.collection("v3").document("data").collection("events").snapshots(),
              builder: (context, snapshot) {
                if (snapshot.hasError)
                  return new Text('Error: ${snapshot.error}');
                switch (snapshot.connectionState) {
                  case ConnectionState.none:
                    return new Text('Vazio');
                  case ConnectionState.waiting:
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  default:
                    if (snapshot.data.documents.length < 1) //vazio
                      return Center(child: Text('Nenhum Evento'),);
                    else
                      return new ListView(
                        children: snapshot.data.documents.reversed.map((DocumentSnapshot document) {
                          String date = "${document['dateEvent']}";
                          String local = "${document['localEvent']}";
                          return _buildBeatfulTileList(
                              document['nameEvent'] ?? 'null',
                              date ?? 'null',
                              local ?? 'null',
                                  (){
                                    Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (BuildContext context) => EventDataHome(
                                      EVENT: EventModel(
                                        idEvent: document['idEvent'] ?? 'null',
                                        dateEvent: document['dateEvent'] ?? 'null',
                                        localEvent: document['localEvent'] ?? 'null',
                                        nameEvent: document['nameEvent'] ?? 'null',
                                        passEvent: document['passEvent'] ?? 'null',
                                      ),
                                    ))
                                );
                              }
                          );
                        }).toList(),
                      );
                }
              },
            ),
          )
      )
    );
  }

  Widget _buildBeatfulTileList(String textTitle, String textSubtitleData, String textSubtitleLocal, Function func){
    return Card(
      color: Colors.black45,
      child: Container(
        child: ListTile(
            onTap: func,
            contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
            leading: Container(
              padding: EdgeInsets.only(right: 12.0),
              decoration: new BoxDecoration(
                  border: new Border(
                      right: new BorderSide(width: 1.0, color: Colors.white24))),
              child: Icon(Icons.event, color: Colors.white),
            ),
            title: Padding(
                padding: EdgeInsets.only(bottom: 5.0),
                child: Text(
                  textTitle,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                  softWrap: false,
                  style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20.0),
                )
            ),
            subtitle: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(right: 5.0),
                      child: Icon(Icons.timer, color: Colors.white, size: 15.0,),
                    ),
                    Flexible(
                        child: Text(textSubtitleData, style: TextStyle(color: Colors.white))
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(right: 5.0),
                      child: Icon(Icons.location_on, color: Colors.white, size: 15.0,),
                    ),
                    Flexible(
                        child: Text(textSubtitleLocal, style: TextStyle(color: Colors.white))
                    )
                  ],
                )
              ],
            ),
            trailing: Icon(Icons.keyboard_arrow_right, color: Colors.white, size: 30.0)),
      ),
    );
  }

  void _openNewEventDialog() {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
        builder: (BuildContext context) {
          return new NewEventDialog();
        },
        fullscreenDialog: true
    ));
  }

  Future _openNewEventDialogWithReturn() async{
    bool save = await Navigator.of(context).push(new MaterialPageRoute<bool>(
        builder: (BuildContext context) {
          return new NewEventDialog();
        },
        fullscreenDialog: true
    ));
    if (save != null) {
      if(save){
        Timer(new Duration(milliseconds: 500), () {
          _scaffoldKey2.currentState.showSnackBar(
              SnackBar(
                  content: Text('Adicionado com sucesso'),
                  duration: const Duration(seconds: 3)
              )
          );
        });
      }
    }
  }
}
