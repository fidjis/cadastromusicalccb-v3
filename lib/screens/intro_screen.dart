import 'package:flutter/material.dart';

class IntroScreen extends StatelessWidget {

  final PageController _pageController = PageController();
  BuildContext _context;

  @override
  Widget build(BuildContext context) {
    _context = context; //para utilizar nos widgets dos metodos
    return PageView(
      onPageChanged: (pageAtual){
        if(pageAtual == 2){
          //_showDialogNeverSatisfied(context);
        }
      },
      controller: _pageController,
//      physics: NeverScrollableScrollPhysics(), //NAO PERMITE O USER ARRASTAR O PAGER
      children: <Widget>[
        _buildIntroScreen(pageNumber: 0, colorChanged: Colors.blue[300], img: "assets/intro_1.png", description: "Gerencie eventos musicais!"),
        _buildIntroScreen(pageNumber: 1, colorChanged: Colors.green[300], img: "assets/intro_2.png", description: "Crie novos eventos relacionados a música!"),
        _buildIntroScreen(pageNumber: 2, colorChanged: Colors.red[300], img: "assets/intro_3.png", description: "Obtenha um relatorio de maneira rapida e facil!")
      ],
    );
  }

  Widget _buildIntroScreen({@required int pageNumber, @required Color colorChanged, @required String img, @required String description}){
    return Scaffold(
      backgroundColor: colorChanged,
      bottomNavigationBar: BottomAppBar(
        elevation: 50.0,
        color: Colors.black54,
        child: _bottomAppBarContents(pageNumber),
      ),
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset(img, height: 100.0,),
            Padding(
                child: Text(
                  description,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.white,
                  ),
                ),
                padding: EdgeInsets.all(10.0)
            ),
          ],
        ),
      ),
    );
  }

  Widget _bottomAppBarContents(int pageNumber){
    
    String text;
    if(pageNumber == 2)
      text = "Clicke Aqui";
    else
      text = "Deslize para o lado";
    
    return ListTile(
      title: Text(
        text,
        style: TextStyle(
            color: Colors.white
        ),
      ),
      trailing: Icon(Icons.keyboard_arrow_right, color: Colors.white,),
      onTap: (){
        if(pageNumber == 2){
          Navigator.pushReplacementNamed(_context, '/HomeScreen'); //ASSIM NAO DEIXA O USUARIO VOLTAR PARA TELA ANTERIOR
        }
      },
    );
  }

  Future<void> _showDialogNeverSatisfied(context) async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Rewind and remember'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('You will never be satisfied.'),
                Text('You\’re like me. I’m never satisfied.'),
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Regret'),
              onPressed: () {

              },
            ),
          ],
        );
      },
    );
  }
}