import 'package:cadastro_musical_ccb_v3/models/event_model.dart';
import 'package:cadastro_musical_ccb_v3/models/person_model.dart';
import 'package:cadastro_musical_ccb_v3/util/autocompletesearch.dart';
import 'package:flutter/material.dart';

class AddMusicoScreen extends StatefulWidget {

  AddMusicoScreen({this.EVENT});

  final EventModel EVENT;

  @override
  _AddMusicoScreenState createState() => _AddMusicoScreenState();
}

class _AddMusicoScreenState extends State<AddMusicoScreen> {

  AutoComplete autoCompleteInstrumento = AutoComplete();

  final _formKey = GlobalKey();
  final _personMusico = Musico();

  bool salvando = false;
  var childSalvando = CircularProgressIndicator();
  var childSalvar = Text(
    'SALVAR',
    style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orangeAccent,
        leading: GestureDetector(
          onTap: (){Navigator.pop(context);},
          child: Icon(Icons.arrow_back_ios, color: Colors.white),
        ),
        bottom: PreferredSize(
            preferredSize: Size(0.0, 130.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Add Músico",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 40.0
                  ),
                ),
                Text(
                  "-",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 50.0
                  ),
                )
              ],
            )
        ),
      ),
      body: ListView(
        children: <Widget>[
          Container(
              padding:
              const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
              child: Builder(
                  builder: (context) => Form(
                      key: _formKey,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [

                            autoCompleteInstrumento, ///TODO: tentar passar um controler para modificar os valores a partir desse contexto
                            SizedBox(height: 4.0,),
                            TextFormField(
//                                controller: searchController,
                              decoration: InputDecoration(
                                hintText: 'Cidade',
                                contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(25.0)),
                              ),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Please enter your city name';
                                }
                              },
                              onSaved: (val) => setState(() => _personMusico.cidade = val),
                            ),
                            SizedBox(height: 4.0,),
                            SwitchListTile(
                                title: const Text('Oficializado?'),
                                value: _personMusico.oficializado,
                                onChanged: (bool val) => setState(() => _personMusico.oficializado = val)
                            ),
                            Container(
                              padding: const EdgeInsets.fromLTRB(0, 25, 0, 20),
                              child: Text('Cargos:'),
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(child: CheckboxListTile(
                                    title: const Text(Person.ENC_LOCAL),
                                    value: _personMusico.cargos[Person.ENC_LOCAL],
                                    onChanged: (val) {
                                      setState(() =>
                                      _personMusico.cargos[Person.ENC_LOCAL] = val);
                                    }),),
                                Expanded(child: CheckboxListTile(
                                    title: const Text(Person.ANCIAO),
                                    value: _personMusico.cargos[Person.ANCIAO],
                                    onChanged: (val) {
                                      setState(() =>
                                      _personMusico.cargos[Person.ANCIAO] = val);
                                    }))
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(child: CheckboxListTile(
                                    title: const Text(Person.INSTRUTOR),
                                    value: _personMusico.cargos[Person.INSTRUTOR],
                                    onChanged: (val) {
                                      setState(() =>
                                      _personMusico.cargos[Person.INSTRUTOR] = val);
                                    })),
                                Expanded(child: CheckboxListTile(
                                    title: const Text(Person.DIACONO),
                                    value: _personMusico.cargos[Person.DIACONO],
                                    onChanged: (val) {
                                      setState(() =>
                                      _personMusico.cargos[Person.DIACONO] = val);
                                    })),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(child: CheckboxListTile(
                                    title: const Text(Person.COOP_OFICIAL),
                                    value: _personMusico.cargos[Person.COOP_OFICIAL],
                                    onChanged: (val) {
                                      setState(() =>
                                      _personMusico.cargos[Person.COOP_OFICIAL] = val);
                                    })),
                                Expanded(child: CheckboxListTile(
                                    title: const Text(Person.COOP_JOVENS),
                                    value: _personMusico.cargos[Person.COOP_JOVENS],
                                    onChanged: (val) {
                                      setState(() =>
                                      _personMusico.cargos[Person.COOP_JOVENS] = val);
                                    })),
                              ],
                            ),
                            CheckboxListTile(
                                title: const Text(Person.ENC_REGIONAL),
                                value: _personMusico.cargos[Person.ENC_REGIONAL],
                                onChanged: (val) {
                                  setState(() => _personMusico.cargos[Person.ENC_REGIONAL] = val);
                                }),
                            SizedBox(height: 10.0,),
                            MaterialButton(
                              color: Colors.orange,
                              minWidth: 160,
                              child: salvando ? childSalvando : childSalvar,
                              onPressed: () async {

                                _personMusico.instrumento = autoCompleteInstrumento.state.searchTextField.textField.controller.value.text;

                                if(!salvando){
                                  if(_personMusico.instrumento != ""){
                                    setState(() {
                                      salvando = true;
                                    });
                                    await _personMusico.save(widget.EVENT.idEvent, context);
                                    setState(() {
                                      salvando = false;
                                      resetPerson();
                                    });
                                  }
                                }

//                                final form = _formKey.currentState;
//                                if (form.validate()) {
//                                  form.save();
//                                  _user.save();
//                                  _showDialog(context);
//                                }
                              },
                            ),

                          ]))))
        ],
      ),
    );
  }

  resetPerson(){
    setState(() {
      autoCompleteInstrumento.state.searchTextField.textField.controller.text = "";
      _personMusico.dataMap = null;
      _personMusico.TAG = "";
      _personMusico.cidade = "";
      _personMusico.instrumento = "";
      _personMusico.cargos = {
        Person.ENC_LOCAL: false,
        Person.ENC_REGIONAL: false,
        Person.ANCIAO: false,
        Person.INSTRUTOR: false,
        Person.DIACONO: false,
        Person.COOP_JOVENS: false,
        Person.COOP_OFICIAL: false,
      };
      _personMusico.oficializado = false;
    });
  }
}


