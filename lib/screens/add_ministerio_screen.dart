import 'package:cadastro_musical_ccb_v3/models/event_model.dart';
import 'package:cadastro_musical_ccb_v3/models/person_model.dart';
import 'package:flutter/material.dart';

class AddMinisterioScreen extends StatefulWidget {

  AddMinisterioScreen({this.EVENT});

  final EventModel EVENT;
  
  @override
  _AddMinisterioScreenState createState() => _AddMinisterioScreenState();
}

class _AddMinisterioScreenState extends State<AddMinisterioScreen> {

  final _formKey = GlobalKey();
  final _personMinisterio = Ministerio();

  bool salvando = false;
  var childSalvando = CircularProgressIndicator();
  var childSalvar = Text(
    'SALVAR',
    style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orangeAccent,
        leading: GestureDetector(
          onTap: (){Navigator.pop(context);},
          child: Icon(Icons.arrow_back_ios, color: Colors.white),
        ),
        bottom: PreferredSize(
            preferredSize: Size(0.0, 130.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Add Ministerio",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 40.0
                  ),
                ),
                Text(
                  "-",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 50.0
                  ),
                )
              ],
            )
        ),
      ),
      body: ListView(
        children: <Widget>[
          Container(
              padding:
              const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
              child: Builder(
                  builder: (context) => Form(
                      key: _formKey,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [

                            TextFormField(
//                                controller: searchController,
                              decoration: InputDecoration(
                                hintText: 'Cidade',
                                contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(25.0)),
                              ),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Please enter your city name';
                                }
                              },
                              onSaved: (val) => setState(() => _personMinisterio.cidade = val),
                            ),
                            Container(
                              padding: const EdgeInsets.fromLTRB(0, 25, 0, 20),
                              child: Text('Cargos:'),
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(child: CheckboxListTile(
                                    title: const Text(Person.ANCIAO),
                                    value: _personMinisterio.cargos[Person.ANCIAO],
                                    onChanged: (val) {
                                      setState(() =>
                                      _personMinisterio.cargos[Person.ANCIAO] = val);
                                    })),
                                Expanded(child: CheckboxListTile(
                                    title: const Text(Person.DIACONO),
                                    value: _personMinisterio.cargos[Person.DIACONO],
                                    onChanged: (val) {
                                      setState(() =>
                                      _personMinisterio.cargos[Person.DIACONO] = val);
                                    }),),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(child: CheckboxListTile(
                                    title: const Text(Person.COOP_OFICIAL),
                                    value: _personMinisterio.cargos[Person.COOP_OFICIAL],
                                    onChanged: (val) {
                                      setState(() =>
                                      _personMinisterio.cargos[Person.COOP_OFICIAL] = val);
                                    })),
                                Expanded(child: CheckboxListTile(
                                    title: const Text(Person.COOP_JOVENS),
                                    value: _personMinisterio.cargos[Person.COOP_JOVENS],
                                    onChanged: (val) {
                                      setState(() =>
                                      _personMinisterio.cargos[Person.COOP_JOVENS] = val);
                                    })),
                              ],
                            ),

                            SizedBox(height: 10.0,),
                            MaterialButton(
                              color: Colors.orange,
                              minWidth: 160,
                              child: salvando ? childSalvando : childSalvar,
                              onPressed: () async {

                                if(!salvando){
                                  setState(() {
                                    salvando = true;
                                  });
                                  await _personMinisterio.save(widget.EVENT.idEvent, context);
                                  setState(() {
                                    salvando = false;
                                    resetPerson();
                                  });
                                }

//                                final form = _formKey.currentState;
//                                if (form.validate()) {
//                                  form.save();
//                                  _user.save();
//                                  _showDialog(context);
//                                }
                              },
                            ),

                          ]))))
        ],
      ),
    );
  }

  resetPerson(){
    setState(() {
      _personMinisterio.dataMap = null;
      _personMinisterio.TAG = "";
      _personMinisterio.cidade = "";
      _personMinisterio.cargos = {
        Person.ANCIAO: false,
        Person.DIACONO: false,
        Person.COOP_JOVENS: false,
        Person.COOP_OFICIAL: false,
      };
    });
  }
}


