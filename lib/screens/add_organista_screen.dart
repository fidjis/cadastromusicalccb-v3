import 'package:cadastro_musical_ccb_v3/models/event_model.dart';
import 'package:cadastro_musical_ccb_v3/models/person_model.dart';
import 'package:flutter/material.dart';

class AddOrganistaScreen extends StatefulWidget {

  AddOrganistaScreen({this.EVENT});

  final EventModel EVENT;

  @override
  _AddOrganistaScreenState createState() => _AddOrganistaScreenState();
}

class _AddOrganistaScreenState extends State<AddOrganistaScreen> {
  final _formKey = GlobalKey<FormState>();
  final _personOrganista = Organista();

  bool salvando = false;
  var childSalvando = CircularProgressIndicator();
  var childSalvar = Text(
    'SALVAR',
    style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orangeAccent,
        leading: GestureDetector(
          onTap: (){Navigator.pop(context);},
          child: Icon(Icons.arrow_back_ios, color: Colors.white),
        ),
        bottom: PreferredSize(
            preferredSize: Size(0.0, 130.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Add Organista",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 40.0
                  ),
                ),
                Text(
                  "-",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 50.0
                  ),
                )
              ],
            )
        ),
      ),
      body: ListView(
        children: <Widget>[
          Container(
              padding:
              const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
              child: Builder(
                  builder: (context) => Form(
                      key: _formKey,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [

                            TextFormField(
//                                controller: searchController,
                              decoration: InputDecoration(
                                hintText: 'Cidade',
                                contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(25.0)),
                              ),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'Please enter your city name';
                                }
                              },
                              onSaved: (val) => setState(() => _personOrganista.cidade = val),
                            ),
                            SizedBox(height: 4.0,),
                            SwitchListTile(
                                title: const Text('Oficializada?'),
                                value: _personOrganista.oficializado,
                                onChanged: (bool val) => setState(() => _personOrganista.oficializado = val)
                            ),
                            SizedBox(height: 4.0,),
                            Container(
                              padding: const EdgeInsets.fromLTRB(0, 25, 0, 20),
                              child: Text('Cargos:'),
                            ),
                            CheckboxListTile(
                                title: const Text(Person.INSTRUTORA),
                                value: _personOrganista.cargos[Person.INSTRUTORA],
                                onChanged: (val) {
                                  setState(() =>
                                  _personOrganista.cargos[Person.INSTRUTORA] = val);
                                }),
                            CheckboxListTile(
                                title: const Text(Person.EXAMINADORA),
                                value: _personOrganista.cargos[Person.EXAMINADORA],
                                onChanged: (val) {
                                  setState(() => _personOrganista.cargos[Person.EXAMINADORA] = val);
                                }),
                            CheckboxListTile(
                                title: const Text(Person.CULTO_OFICIAL),
                                value: _personOrganista.cargos[Person.CULTO_OFICIAL],
                                onChanged: (val) {
                                  setState(() =>
                                  _personOrganista.cargos[Person.CULTO_OFICIAL] = val);
                                }),
                            CheckboxListTile(
                                title: const Text(Person.RJM),
                                value: _personOrganista.cargos[Person.RJM],
                                onChanged: (val) {
                                  setState(() => _personOrganista.cargos[Person.RJM] = val);
                                }),
                            SizedBox(height: 10.0,),



                            MaterialButton(
                              color: Colors.orange,
                              minWidth: 160,
                              child: salvando ? childSalvando : childSalvar,
                              onPressed: () async {

                                if(!salvando){
                                  setState(() {
                                    salvando = true;
                                  });

                                  await _personOrganista.save(widget.EVENT.idEvent, context);

//                                  final form = _formKey.currentState;
//                                  if (form.validate()) {
//                                    form.save();
//                                    await _personOrganista.save(widget.EVENT.idEvent, context);
//                                  }

                                  setState(() {
                                    salvando = false;
                                    resetPerson();
                                  });
                                }
                              },
                            ),

                          ]))))
        ],
      ),
    );
  }

  resetPerson(){
    setState(() {
      _personOrganista.dataMap = null;
      _personOrganista.TAG = "";
      _personOrganista.cidade = "";
      _personOrganista.instrumento = "";
      _personOrganista.cargos = {
        Person.EXAMINADORA: false,
        Person.INSTRUTORA: false,
        Person.CULTO_OFICIAL: false,
        Person.RJM: false,
      };
      _personOrganista.oficializado = false;
    });
  }
}