import 'package:cadastro_musical_ccb_v3/models/event_model.dart';
import 'package:flutter/material.dart';

class ConfigScreen extends StatefulWidget {

  ConfigScreen({this.EVENT});

  final EventModel EVENT;

  @override
  _ConfigScreenState createState() => _ConfigScreenState();
}

class _ConfigScreenState extends State<ConfigScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orangeAccent,
        leading: GestureDetector(
          onTap: (){Navigator.pop(context);},
          child: Icon(Icons.arrow_back_ios, color: Colors.white),
        ),
        bottom: PreferredSize(
            preferredSize: Size(0.0, 130.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Add Ministerio",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 40.0
                  ),
                ),
                Text(
                  "-",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 50.0
                  ),
                )
              ],
            )
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Icon(Icons.warning, color: Colors.red,),
              Text("  Em Construção!", style: TextStyle(fontSize: 25.0, color: Colors.red,))
            ],
          )
        ],
      ),
    );
  }
}
