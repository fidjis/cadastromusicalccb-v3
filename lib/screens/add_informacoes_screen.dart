import 'package:cadastro_musical_ccb_v3/models/event_info_data.dart';
import 'package:cadastro_musical_ccb_v3/models/event_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class AddInformacoesScreen extends StatefulWidget {

  AddInformacoesScreen({this.EVENT});

  final EventModel EVENT;

  @override
  _AddInformacoesScreenState createState() => _AddInformacoesScreenState();
}

class _AddInformacoesScreenState extends State<AddInformacoesScreen> {

  final _formKey = GlobalKey<FormState>();
  EventInfo _eventInfo = EventInfo();
  TextEditingController controllerAnciaoName =  TextEditingController();
  TextEditingController controllerEncarregadoName =  TextEditingController();
  TextEditingController controllerPalavra =  TextEditingController();

  bool salvando = false;
  var childSalvando = CircularProgressIndicator();
  var childSalvar = Text(
    'SALVAR',
    style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
  );

  @override
  initState(){
    super.initState();
    getEventInf();
  }

  getEventInf() async {
    await _eventInfo.getEventInf(widget.EVENT.idEvent);
    setState((){}); //pra atualizar tudo
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orangeAccent,
        leading: GestureDetector(
          onTap: (){Navigator.pop(context);},
          child: Icon(Icons.arrow_back_ios, color: Colors.white),
        ),
        bottom: PreferredSize(
            preferredSize: Size(0.0, 130.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Add Informações",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 40.0
                  ),
                ),
                Text(
                  "-",
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 50.0
                  ),
                )
              ],
            )
        ),
      ),
      body: ListView(
        children: <Widget>[
          Container(
              padding:
              const EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
              child: Builder(
                  builder: (context) => Form(
                    autovalidate: true,
                      key: _formKey,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [

                            Text("Ancião: ${_eventInfo.anciaoName ?? "..."}"),
                            Text("Encarregado: ${_eventInfo.engarregado ?? "..."}"),
                            Text("Palavra: ${_eventInfo.palavra ?? "..."}"),

                            Container(
                              padding: const EdgeInsets.fromLTRB(0, 25, 0, 20),
                              child: Text('Novas informações:'),
                            ),
                            TextFormField(
                              controller: controllerAnciaoName,
                              decoration: InputDecoration(
                                hintText: 'Ancião',
                                contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(25.0)),
                              ),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return '';
                                }
                              },
                              onSaved: (val) => setState(() => _eventInfo.anciaoName = val),
                            ),
                            SizedBox(height: 10.0,),
                            TextFormField(
                              controller: controllerEncarregadoName,
                              decoration: InputDecoration(
                                hintText: 'Encarregado',
                                contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(25.0)),
                              ),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return '';
                                }
                              },
                              onSaved: (val) => setState(() => _eventInfo.engarregado = val),
                            ),
                            SizedBox(height: 10.0,),
                            TextFormField(
                              controller: controllerPalavra,
                              decoration: InputDecoration(
                                hintText: 'Palavra',
                                contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
                                border: OutlineInputBorder(borderRadius: BorderRadius.circular(25.0)),
                              ),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return '';
                                }
                              },
                              onSaved: (val) => setState(() => _eventInfo.palavra = val),
                            ),

                            SizedBox(height: 20.0,),
                            MaterialButton(
                              color: Colors.orange,
                              minWidth: 160,
                              child: salvando ? childSalvando : childSalvar,
                              onPressed: () async {

                                if(!salvando){
                                  setState(() {
                                    salvando = true;
                                  });
                                  final form = _formKey.currentState;
                                  if (form.validate()) {
                                    form.save();
                                    await _eventInfo.save(widget.EVENT.idEvent, context);
                                  }
                                  setState(() {
                                    salvando = false;
                                  });
                                }

                              },
                            ),

                          ]))))
        ],
      ),
    );
  }
}
