import 'dart:async';
import 'dart:collection';

import 'package:cadastro_musical_ccb_v3/util/my_personal_widgets.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:cadastro_musical_ccb_v3/models/event_model.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class NewEventDialog extends StatefulWidget {
  @override
  NewEventDialogState createState() => new NewEventDialogState();
}

class NewEventDialogState extends State<NewEventDialog> {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = true; //SO VAI VALIDAR QUANDO ATIVAR O TRUE (NO TEXTFORMFILD OU EM ALGUM METODO)

  String _dateEventString = 'Data';
  String _nameEvent;
  String _passEvent;
  String _idEvent;
  String _localEvent;

  TextEditingController _nameEventController;
  TextEditingController _passEventController;
  TextEditingController _localEventController;

  @override
  void initState() {
    _nameEventController = new TextEditingController(text: _nameEvent);
    _nameEventController = new TextEditingController(text: _passEvent);
    _nameEventController = new TextEditingController(text: _localEvent);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        key: _scaffoldKey,
      appBar: new AppBar(
        backgroundColor: Colors.orangeAccent,
        title: const Text('Novo Evento'),
        actions: [
          new FlatButton(
              onPressed: () async {
                if (_formKey.currentState.validate()) {
                  //If all data are correct then save data to out variables
                  _formKey.currentState.save();

                  if(_dateEventString != 'Data'){

                    //para mostrar o progress
                    MyWidgets.showProgress1(context);

                    var doc = await Firestore.instance.collection("v3").document("data").collection("events").document();
                    _idEvent = doc.documentID;
                    EventModel data = EventModel(
                        passEvent: _passEvent,
                        nameEvent: _nameEvent,
                        localEvent: _localEvent,
                        dateEvent: _dateEventString,
                        idEvent: _idEvent
                    );
                    HashMap<String, String> mapData = data.getEventMap();

                    await doc.setData(mapData)
                        .whenComplete((){

                          Navigator.of(context).pop(); //fechando primeiro o progressdialog
                          Navigator.of(context).pop(true); //ENVIANDO O RETURNO DE QUE FOI ADICIONADO COM SUCESSO

                    }).catchError((){ //TODO: TEM ALGUM ERRO AQUI AO INCERIR O EVENTO
                          Navigator.of(context).pop(); //fechando primeiro o progressdialog
                          _scaffoldKey.currentState.showSnackBar(
                              SnackBar(
                                  content: Text('ERRO AO TENTAR SALVAR'),
                                  duration: const Duration(seconds: 5)
                              )
                          );
                    });
                  }else{
                    _scaffoldKey.currentState.showSnackBar(SnackBar(
                      content: Text('Informe a Data'),
                      action: SnackBarAction(label: "Ok", onPressed: ()=> _scaffoldKey.currentState.hideCurrentSnackBar()),
                    ));
                  }
                } else {
                  //If all data are not valid then start auto validation.
                  setState(() {
                    _autoValidate = true;
                  });
                }
              },
              child: new Text("SALVAR",
                  style: Theme
                      .of(context)
                      .textTheme
                      .subhead
                      .copyWith(color: Colors.white))),
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Form(
                key: _formKey,
                autovalidate: _autoValidate,
                child: Column(
                  children: <Widget>[
                    _buildEventFomr(hintText: "Nome do Evento", icon: Icons.title, controler: _nameEventController, formItemID: 1),
                    _buildEventFomr(hintText: "Senha do Evento", icon: Icons.assignment_late, controler: _passEventController, formItemID: 2),
                    _buildEventFomr(hintText: "Local/Cidade do Evento", icon: Icons.location_on, controler: _localEventController, formItemID: 3),
                    Card(
                        child: Padding(
                            padding: EdgeInsets.only(top: 10.0, bottom: 10.0, right: 10.0, left: 10.0),
                            child: Row(
                              children: <Widget>[
                                //DATEPIKER ANTIGO
//                                Expanded(
//                                    child: ListTile(
//                                      leading: new Icon(Icons.today, color: Colors.grey[500]),
//                                      title: new DateTimeItem(
//                                        dateTime: _dateEvent, //mudando estado da variavel
//                                        onChanged: (dateTime) => setState(() => _dateEvent = dateTime), //mudando estado da variavel
//                                      ),
//                                    )
//                                )
                                Expanded(
                                    child: ListTile(
                                      leading: new Icon(Icons.today, color: Colors.grey[500]),
                                      title: Text(_dateEventString),
                                      onTap: _showDatePicker,
                                    )
                                ),
                              ],
                            )
                        )
                    ),
                  ],
                )
            ),
          ],
        ),
      )
    );
  }

  Widget _buildEventFomr({@required int formItemID, @required IconData icon, @required String hintText, @required TextEditingController controler}){
    return Card(
        child: Padding(
            padding: EdgeInsets.only(top: 10.0, bottom: 10.0, right: 10.0, left: 10.0),
            child: Row(
              children: <Widget>[
                Expanded(
                    child: ListTile(
                      leading: new Icon(icon, color: Colors.grey[500]),
                      title: TextFormField(
                        keyboardType: TextInputType.text, //definir para o passworld
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 20.0,
                        ),
                        decoration: InputDecoration.collapsed(hintText: hintText),
                        controller: controler, //subtituido pela logica do validator
                        validator: (value){
                          if(value.trim().isEmpty)
                            return "Insira um valor!";
                        },
                        onSaved: (value){
                          switch(formItemID){
                            case 1:
                              _nameEvent = value;
                              break;
                            case 2:
                              _passEvent = value;
                              break;
                            case 3:
                              _localEvent = value;
                              break;
                          }
                        },
                        //onChanged: (value) => _passEvent = value, //é do TextField
                      ),
                    ),
                )
              ],
            )
        )
    );
  }

  bool _showTitleActions = true;
  void _showDatePicker() {
//    final bool showTitleActions = false;
    DatePicker.showDatePicker(
      context,
      showTitleActions: _showTitleActions,
      minYear: DateTime.now().year,
      maxYear: DateTime.now().year + 1,
      initialYear: DateTime.now().year,
      initialMonth: DateTime.now().month,
      initialDate: DateTime.now().day,
      confirm: Text(
        'Confimar data',
        style: TextStyle(color: Colors.cyan),
      ),
      cancel: Text(
        'Cancelar',
        style: TextStyle(color: Colors.red),
      ),
      locale: 'us',
      dateFormat: 'dd-mm-yyyy',
//      onChanged: (year, month, date) {
//        debugPrint('onChanged date: $year-$month-$date');
//
//        if (!showTitleActions) {
//          _changeDatetime(year, month, date);
//        }
//      },
      onConfirm: (year, month, date) {
        setState(() {
          _dateEventString = "$date - $month - $year";
        });
      },
    );
  }
}

//RESPONSAVEL POR CONSTRUIR O DATEPIKE
class DateTimeItem extends StatelessWidget {
  DateTimeItem({Key key, DateTime dateTime, @required this.onChanged})
      : assert(onChanged != null),
        date = dateTime == null
            ? new DateTime.now()
            : new DateTime(dateTime.year, dateTime.month, dateTime.day),
        time = dateTime == null
            ? new DateTime.now()
            : new TimeOfDay(hour: dateTime.hour, minute: dateTime.minute),
        super(key: key);

  final DateTime date;
  final TimeOfDay time;
  final ValueChanged<DateTime> onChanged;

  @override
  Widget build(BuildContext context) {
    return new Row(
      children: <Widget>[
        new Expanded(
          child: new InkWell(
            onTap: (() => _showDatePicker(context)),
            child: new Padding(
                padding: new EdgeInsets.symmetric(vertical: 8.0),
                //child: new Text(new DateFormat('EEEE, MMMM d').format(date))),
                child: new Text(date.toIso8601String())),
          ),
        ),
      ],
    );
  }

  Future _showDatePicker(BuildContext context) async {
    DateTime dateTimePicked = await showDatePicker(
      //locale: Locale("pt_BR"),
        context: context,
        initialDate: date,
        firstDate: date.subtract(const Duration(days: 20000)),
        lastDate: new DateTime.now());

    if (dateTimePicked != null) {
      onChanged(new DateTime(dateTimePicked.year, dateTimePicked.month,
          dateTimePicked.day, time.hour, time.minute));
    }
  }
}