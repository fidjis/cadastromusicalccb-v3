import 'dart:convert';

import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AutoComplete extends StatefulWidget {

  _AutoCompleteState state = new _AutoCompleteState();

  @override
  _AutoCompleteState createState() => state;
}

class _AutoCompleteState extends State<AutoComplete> {
  GlobalKey<AutoCompleteTextFieldState<String>> key = new GlobalKey();

  AutoCompleteTextField searchTextField;

  TextEditingController controller = new TextEditingController();

  _AutoCompleteState();

  List<String> instumentos;

  @override
  void initState() {
    loadInstruments();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
      searchTextField = AutoCompleteTextField<String>(
          style: TextStyle(color: Colors.black, fontSize: 16.0),
          decoration: InputDecoration(
            hintText: 'Instrumento',
            contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
            border: OutlineInputBorder(borderRadius: BorderRadius.circular(25.0)),
          ),
          itemSubmitted: (instrumentName) {
            setState(() => searchTextField.textField.controller.text = instrumentName);
          },
          clearOnSubmit: false,
          key: key,
          suggestions: instumentos,
          itemBuilder: (context, instrumentName) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(7.0),
                  child: Text(
                      instrumentName,
                      style: TextStyle(
                          fontSize: 16.0
                      )),
                )
              ],
            );
          },
          itemSorter: (a, b) {
            return a.compareTo(b);
          },
          itemFilter: (item, query) {
            return item.toLowerCase().startsWith(query.toLowerCase());
          }),
    ]);
  }

  Future loadInstruments() async {
    try {
      instumentos = new List<String>();
      String jsonString = await rootBundle.loadString('assets/instruments.json');
      List parsedJson = json.decode(jsonString);
      for (int i = 0; i < parsedJson.length; i++) {
        instumentos.add(parsedJson[i]);
      }
    } catch (e) {
      print(e);
    }
  }
}