import 'dart:async';

import 'package:cadastro_musical_ccb_v3/screens/add_ministerio_screen.dart';
import 'package:cadastro_musical_ccb_v3/screens/add_musico_screen.dart';
import 'package:cadastro_musical_ccb_v3/screens/add_organista_screen.dart';
import 'package:cadastro_musical_ccb_v3/screens/add_informacoes_screen.dart';
import 'package:cadastro_musical_ccb_v3/screens/config_screen.dart';
import 'package:cadastro_musical_ccb_v3/screens/event_data_home.dart';
import 'package:cadastro_musical_ccb_v3/screens/home_screen.dart';
import 'package:cadastro_musical_ccb_v3/screens/intro_screen.dart';
import 'package:cadastro_musical_ccb_v3/screens/privacy_policy_screen.dart';
import 'package:cadastro_musical_ccb_v3/screens/splash_screnn.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  bool home = true;

  Future initState() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    bool firstUsage = (prefs.getInt('firstUsage') ?? false);
    if(firstUsage){
      home = false;
    }

    await prefs.setBool('firstUsage', false);
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    ///TRAVANDO NO MODO PORTRAIT
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown
    ]);

    return MaterialApp(
        title: 'Cadastro Musical CCB',
        theme: ThemeData(
            primarySwatch: Colors.blue,
            primaryColor: Color.fromARGB(255, 4, 125, 141)
        ),
        debugShowCheckedModeBanner: false, //SEM O BANNER DE DEBUG
        home: Splash(),
//        home: IntroScreen(),
        routes: <String, WidgetBuilder>{
          '/HomeScreen': (BuildContext context) => new HomeScreen(),
          '/IntroScreen': (BuildContext context) => new IntroScreen(),
          '/PrivacyPolictScreen': (BuildContext context) => new PrivacyPolictScreen(),
//          '/ConfigScreen': (BuildContext context) => new ConfigScreen(),
//          '/EventDataHome': (BuildContext context) => new EventDataHome(),
//          '/AddMusicoScreen': (BuildContext context) => new AddMusicoScreen(),
//          '/AddInformacoesScreen': (BuildContext context) => new AddInformacoesScreen(),
//          '/AddOrganistaScreen': (BuildContext context) => new AddOrganistaScreen(),
//          '/AddMinisterioScreen': (BuildContext context) => new AddMinisterioScreen(),
        },
    );
  }
}
